import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ClipboardModule } from 'ngx-clipboard';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ColorListComponent } from './pages/color-list/color-list.component';
import { FullColorListComponent } from './pages/full-color-list/full-color-list.component';
import {ComponentsModule } from './components/components.module';
import { CoverCardsComponent } from './pages/cover-cards/cover-cards.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    AppComponent,
    ColorListComponent,
    FullColorListComponent,
    CoverCardsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ClipboardModule,
    ComponentsModule,
    FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
