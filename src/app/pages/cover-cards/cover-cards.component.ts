import { Component, OnInit } from '@angular/core';
import axios from 'axios';
import WOW from 'wow.js';
import { faSpinner, faExclamationTriangle, faTimesCircle, faAngleDoubleLeft, faAngleDoubleRight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-cover-cards',
  templateUrl: './cover-cards.component.html',
  styleUrls: ['./cover-cards.component.scss']
})
export class CoverCardsComponent implements OnInit {
  faSpinner = faSpinner;
  faExclamationTriangle = faExclamationTriangle;
  faTimesCircle = faTimesCircle;
  faAngleDoubleLeft = faAngleDoubleLeft;
  faAngleDoubleRight = faAngleDoubleRight;
  public loading = false;
  public currentPage = 2;
  public colors = [];
  public selectedColor: any;
  public activeColor = false;
  showCoppied(cardID){
    const self = this;
    let searchingID;
    for(searchingID = 0; searchingID < self.colors.length; searchingID++){
      if(self.colors[searchingID].id == cardID){
        self.colors[searchingID].clicked = true
      }else{
        self.colors[searchingID].clicked = false
      }
    }
  }
  colorClasses(){
    if(this.error == true || this.loading == true){
      return 'w-100 custom-vh2'
    }else{
      return 'w-100'
    }
  }

  public error = false
  readColors(){
    const self = this;
    if(this.currentPage == 2){
      this.currentPage = 1
    }else if(this.currentPage == 1){
      this.currentPage = 2
    }
    self.loading = true
    axios({
      method: 'get',
      url: 'https://reqres.in/api/colors?page='+self.currentPage,
      timeout:10000,
    }).then(function(response){
      self.loading = false
      self.colors = response.data.data
    }).catch(function(error){
      self.loading = false
      self.error = true
    })
  }

  ngOnInit(): void {
    new WOW().init();
    this.readColors();
    this.selectedColor = {}
  }
  


}
