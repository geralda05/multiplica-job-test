import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoverCardsComponent } from './cover-cards.component';

describe('CoverCardsComponent', () => {
  let component: CoverCardsComponent;
  let fixture: ComponentFixture<CoverCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoverCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoverCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
