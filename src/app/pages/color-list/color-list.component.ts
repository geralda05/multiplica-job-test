import { Component, OnInit } from '@angular/core';
import axios from 'axios';
import WOW from 'wow.js';
import { faSpinner, faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-color-list',
  templateUrl: './color-list.component.html',
  styleUrls: ['./color-list.component.scss']
})

export class ColorListComponent implements OnInit {
  faSpinner = faSpinner;
  faExclamationTriangle = faExclamationTriangle;

  constructor() { }
  public currentPage = 2;
  public colors = [];
  
  showCoppied(cardID){
    const self = this;
    let searchingID;
    for(searchingID = 0; searchingID < self.colors.length; searchingID++){
      if(self.colors[searchingID].id == cardID){
        self.colors[searchingID].clicked = true
      }else{
        self.colors[searchingID].clicked = false
      }
    }
  }

  public loading = false;
  public error = false;
  readColors(){
    const self = this;
    if(this.currentPage == 2){
      this.currentPage = 1
    }else if(this.currentPage == 1){
      this.currentPage = 2
    }
    this.loading = true;
    this.error = false;
    axios({
      method: 'get',
      url: 'https://reqres.in/api/colors?page='+self.currentPage,
      timeout:10000,
    }).then(function(response){
      self.loading = false
      self.error = false;
      self.colors = response.data.data
    }).catch(function(error){
      self.loading = false
      self.error = true
    })
  }
  
  ngOnInit(): void {
    new WOW().init();
    this.readColors()
  }
  

}
