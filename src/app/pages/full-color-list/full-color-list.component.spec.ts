import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FullColorListComponent } from './full-color-list.component';

describe('FullColorListComponent', () => {
  let component: FullColorListComponent;
  let fixture: ComponentFixture<FullColorListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FullColorListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullColorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
