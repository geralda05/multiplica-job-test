import { Component, OnInit } from '@angular/core';
import { faSpinner, faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import WOW from 'wow.js';

@Component({
  selector: 'app-full-color-list',
  templateUrl: './full-color-list.component.html',
  styleUrls: ['./full-color-list.component.scss']
})

export class FullColorListComponent implements OnInit {
  faSpinner = faSpinner;
  faExclamationTriangle = faExclamationTriangle;
  constructor() { }
  public currentPage = 2;
  public colors = [];
  
  showCoppied(cardID){
    const self = this;
    let searchingID;
    for(searchingID = 0; searchingID < self.colors.length; searchingID++){
      if(self.colors[searchingID].id == cardID){
        self.colors[searchingID].clicked = true
      }else{
        self.colors[searchingID].clicked = false
      }
    }
  }
  public error = false;
  public loading = false;
  readColors(){
    const self = this;
    if(this.currentPage == 2){
      this.currentPage = 1
    }else if(this.currentPage == 1){
      this.currentPage = 2
    }
    self.loading = true;
    self.error = false;
    axios({
      method: 'get',
      url: 'https://reqres.in/api/colors?page='+self.currentPage,
      timeout:10000,
    }).then(function(response){
      self.loading = false
      self.colors = response.data.data
      self.error = false;
    }).catch(function(error){
      self.loading = false
      self.error = true
    })

  }

  ngOnInit(): void {
    new WOW().init();
    this.readColors()
  }

}
