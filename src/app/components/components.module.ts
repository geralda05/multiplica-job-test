import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from '../app-routing.module';
import { ColorBoxComponent } from './color-box/color-box.component';
import { ClipboardModule } from 'ngx-clipboard';
import { HeaderComponent } from './header/header.component';
import { CardColorComponent } from './card-color/card-color.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [ColorBoxComponent, HeaderComponent, CardColorComponent],
  imports: [
    CommonModule,
    ClipboardModule,
    AppRoutingModule,
    FontAwesomeModule
  ],
  exports:[HeaderComponent, CardColorComponent]
})

export class ComponentsModule { }
