import { Component, OnInit, Input } from '@angular/core';
import { faCopy } from '@fortawesome/free-regular-svg-icons';
import { faClipboardCheck } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'card-color',
  templateUrl: './card-color.component.html',
  styleUrls: ['./card-color.component.scss']
})
export class CardColorComponent implements OnInit {
  faCopy = faCopy;
  faClipboardCheck = faClipboardCheck;
  
  @Input() color;

  constructor() { }

  ngOnInit(): void {
  }

}
