import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ColorListComponent } from './pages/color-list/color-list.component';
import { FullColorListComponent } from './pages/full-color-list/full-color-list.component';
import { CoverCardsComponent } from './pages/cover-cards/cover-cards.component'
const routes: Routes = [
  {
      path: '',
      redirectTo:'color-list',
      pathMatch: 'full'
  },
  {
    path:'color-list',
    component: ColorListComponent,
  },
  {
    path:'full-color-list',
    component: FullColorListComponent,
  },
  {
    path:'cover-cards',
    component: CoverCardsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }