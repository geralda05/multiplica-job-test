# Multiplica Test for Job Admission
## Made by Gerald Leonel Alarcón, Chile 05/2020

`https://bitbucket.org/geralda05/multiplica-job-test/`

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.2.
Application created with the purpose of obtaining an array from API color and thus create a dynamic color palette where developers or users can copy the HEX color codes just by clicking on them. In this, 3 versions of the code were developed, 
- One in the route `/cover-cards` where it was developed exactly as it is in the wireframe. 
- In the route `/color-list` a more compact version, versatile and with not so pronounced actions. 
- Finally the route `/full-color-list` where the compact version is detailed but with a custom grid using flexbox, where the containers maintain their size in all resolutions. It can be seen mostly on screens +desktop. However, all the version were made in responsive mode, for run in all resolutions as mobile, desktop and tablet.

The application contains a header where you can navigate between all versions easily.


## Frameworks used
- Bootstrap
- Git for versioning and code order
- Axios for handle data using HTTP
- Node.js/Express.js/Heroku for deployment
- Wow.js for animations
- Scss for handle styles
- NgxClipboard for copy data to clipboard
- Fontawesome for icons

## Development server
Actually is deployed and available on `http://multiplica-job-admission.herokuapp.com/`.

For run locally in development server, please clone the repository available on `https://bitbucket.org/geralda05/multiplica-job-test/` and in the root folder, run `npm install` for install dependences, finally run `ng serve` and after compiling navigate to `http://localhost:4200/`.



# Prueba de Admisión Laboral para Multiplica
## Hecho por Gerald Leonel Alarcón, Chile 05/2020

Este proyecto se generó con [Angular CLI] (https://github.com/angular/angular-cli) versión 9.1.2.
Aplicación creada con el propósito de obtener un array de colores de una API y así crear una paleta de colores dinámica donde los desarrolladores o usuarios pueden copiar los códigos de color HEX simplemente haciendo clic en ellos. Para ello, se desarrollaron 3 versiones del código,

- Uno en la ruta `/cover-cards` donde se desarrolló exactamente como está en la estructura metálica.
- En la ruta `/color-list` una versión más compacta, versátil y con animaciones no tan pronunciadas.
- Finalmente, la ruta `/full-color-list` donde se detalla la versión compacta pero con un layout-grid personalizado usando flexbox, donde los contenedores mantienen su tamaño en todas las resoluciones. Se puede ver principalmente en pantallas con resoluciones +desktop. Aun asi, todas las versiones fueron desarrolladas en modo responsive para verse en todas las resoluciones (mobile, tablet, desktop).

La aplicación contiene un encabezado donde puede navegar fácilmente entre todas las versiones.


## Frameworks utilizados
- Bootstrap
- Git para versionamiento y orden de código
- Axios para manejar datos usando peticiones HTTP
- Node.js / Express.js / Heroku para implementación
- Wow.js para animaciones
- Scss para manejar estilos
- NgxClipboard para copiar datos al portapapeles
- Fontawesome para iconos

## Servidor de desarrollo
Actualmente está implementado y disponible en `http://multiplica-job-admission.herokuapp.com/`.

Para ejecutar localmente en el servidor de desarrollo, por favor clonar el repositorio ubicado en `https://bitbucket.org/geralda05/multiplica-job-test/`, una vez descargado dentro de la carpeta raiz ejecute `npm install` para las dependencias de instalación, finalmente ejecute`ng serve` y luego de compilar navegue a `http://localhost:4200/`.